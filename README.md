Senku
=========
A peg solitaire game for any web browser.

It is made in HTML5, CSS3 and JavaScript.

[Play it here!](https://americotoledano.gitlab.io/senku/)


Screenshot
--------------
![Screenshot](img/screenshot.png "Screenshot")
